python-cdo (1.6.0-4) unstable; urgency=medium

  * Team Upload
  * Reformat d/control with debputy
  * Add runtime dependency on python3-pkg-resources
    (it was removed from python3-numpy)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 07 Mar 2025 11:23:04 +0100

python-cdo (1.6.0-3) unstable; urgency=medium

  * Set Debian Science Maint. as maintainer
  * Fix broken vcs url
  * Standards-Version: 4.7.0; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 14 Feb 2025 15:58:55 +0000

python-cdo (1.6.0-2) unstable; urgency=medium

  * Push to unstable

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 11 Jun 2023 12:07:40 +0100

python-cdo (1.6.0-1) experimental; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in debian/watch.
  * Fix broken Vcs URL.
  * Fix day-of-week for changelog entry 1.0.9-1.

  [ Alastair McKinstry ]
  * New upstream release
  * Update standards version to 4.6.2, no changes needed.

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 29 May 2023 16:59:42 +0100

python-cdo (1.5.7-1) unstable; urgency=medium

  * New upstream release
  * Drop version.patch
  * Standards-Version: 4.6.1

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 18 Oct 2022 11:14:40 +0100

python-cdo (1.5.6-1) unstable; urgency=medium

  * Standards-Version: 4.6.0
  * New upstream release
  * Patch to correct version number

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 10 Jan 2022 13:58:31 +0000

python-cdo (1.5.5-1) unstable; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 09 Aug 2021 07:07:00 +0100

python-cdo (1.5.4-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.5.1
  * Homepage now https://pypi.org/project/cdo/
  * Use debhelper-compat (=13)
  * use dh-sequence-pyhton3 rather than dh_python
  * Add d/gbp.conf to reflect DEP-14 branch names
  * Use -b debian/latest in VCS-* ref

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 26 Dec 2020 12:59:32 +0000

python-cdo (1.5.3-1) unstable; urgency=medium

  * New upstream release
  * Drop dependency on python-numpy
  * Move to debhelper12; use pybuild
  * Add dependencies on python3-xarray for build tests
  * Add dependencies on python3-netcdf4

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 17 Jun 2019 12:19:37 +0100

python-cdo (1.5.2-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.3.0
  * Drop hard-coded 'xz' compression

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 17 Feb 2019 10:00:32 +0000

python-cdo (1.4.0-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version now 4.2.1
  * Drop obsolete X-Python-* lines
  * Depend on python3-xarray, dont just Recommend.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 21 Sep 2018 12:12:34 +0100

python-cdo (1.3.6-1) unstable; urgency=medium

  * New upstream release

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 04 Apr 2018 06:41:28 +0100

python-cdo (1.3.5-3) unstable; urgency=medium

  * Point VCS to salsa.debian.org
  * Standards-Version: 4.1.3; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 08 Feb 2018 13:01:17 +0000

python-cdo (1.3.5-2) unstable; urgency=medium

  * Change dependency on xarray to recommends; gets past FTBFS on archs where
    pandas currently not present. Functionality is optional

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 08 Nov 2017 09:08:34 +0000

python-cdo (1.3.5-1) unstable; urgency=medium

  * New upstream release
  * python3-cdo requires python3-xarray
  * Drop python-cdo as python-xarray not shipped in  Debian Buster
  * Standards-Version: 4.1.1
  * Patch needed to change version to 1.3.5
  * Delete obsolete avoid_szip.patch
  * Enable tests again: require cdo to do so.
  * Change Priority: extra to optional

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 04 Oct 2017 14:26:47 +0100

python-cdo (1.3.4-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version: 4.1.0; no changes required

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 09 Sep 2017 15:11:18 +0100

python-cdo (1.3.3-1) unstable; urgency=medium

  * New upstream release
  * DH_COMPAT=10

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 28 Oct 2016 10:40:48 +0100

python-cdo (1.3.0-2) unstable; urgency=medium

  * Move to standards-Version: 3.9.8

 -- Alastair McKinstry <mckinstry@debian.org>  Mon, 08 Aug 2016 16:13:13 +0100

python-cdo (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * Close old bug: Closes: #718832.

 -- Alastair McKinstry <mckinstry@debian.org>  Tue, 26 Jan 2016 16:28:19 +0000

python-cdo (1.2.6-1) unstable; urgency=medium

  * New upstream 1.2.6
  * Move to Standards-Version: 3.9.6
  * Add dh-python to Build-Depends.
  * Add Vcs-Git: debian-science.
  * Add redirection-based watch file for pypi.

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 21 Jun 2015 16:18:01 +0100

python-cdo (1.2.1-7) unstable; urgency=medium

  *  Remove unnecessary dependencies on python*-dev packages. Instead use
     python*-all.
     Closes: #718832.
  * Use xz compression.
  * Now at Standards-Version: 3.9.5

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 15 Jun 2014 19:27:44 +0100

python-cdo (1.2.1-6) unstable; urgency=low

  * Python3 support: python3-cdo added.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 10 Jul 2013 15:38:28 +0100

python-cdo (1.2.1-5) unstable; urgency=low

  * Remove dependencies on python-support, cdo. Not used.
  * Remove autogenerated files so that it builds twice-in-a-row

 -- Alastair McKinstry <mckinstry@debian.org>  Sun, 19 May 2013 06:29:46 +0100

python-cdo (1.2.1-4) unstable; urgency=low

  * Disable testing. Do via DEP-8 later instead.

 -- Alastair McKinstry <mckinstry@debian.org>  Sat, 26 Jan 2013 14:12:04 +0000

python-cdo (1.2.1-3) unstable; urgency=low

  * Add dependency on cdo, needed for testing.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 18 Jan 2013 14:14:56 +0000

python-cdo (1.2.1-2) unstable; urgency=low

  * Use python-all-dev rather than python-dev to ensure all python versions
    are included for testing.

 -- Alastair McKinstry <mckinstry@debian.org>  Fri, 18 Jan 2013 13:02:41 +0000

python-cdo (1.2.1-1) unstable; urgency=low

  * New upstream release.
  * Move to debhelper >=9 dependency and Standards-Version: 3.9.4;
    no changes required.
  * Run the tests/ test suite from debian/rules testing target.

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 16 Jan 2013 21:46:06 +0000

python-cdo (1.0.9-2) unstable; urgency=low

  * Use clean pbuilder cache when building. Add setuptools to B-D.
    Closes: #672502.

 -- Alastair McKinstry <mckinstry@debian.org>  Thu, 10 May 2012 19:02:21 +0100

python-cdo (1.0.9-1) unstable; urgency=low

  * Initial release. (Closes: #660347)

 -- Alastair McKinstry <mckinstry@debian.org>  Wed, 25 Apr 2012 10:17:37 +0000
